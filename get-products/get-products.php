<?php

/**
 *    Plugin name: Get products
 *
 */

function _add_product($str)
{
    $str = explode('|', $str);
    $url = $str[0];
    $cat = isset($str[1]) ? $str[1] : '';
    $cat = explode(',', $cat);
    $cats = array();
    foreach ($cat AS $_cat) {
        $catID = get_term_by('name', $_cat, 'category');
        if ($catID != false) {
            $cats[] = $catID->term_id;
        } else {
            $catID = wp_insert_term($_cat, 'category');
            if (!is_wp_error($catID) && is_array($catID)) {
                $cats[] = $catID['term_id'];
            }
        }
    }
    $tag = isset($str[2]) ? $str[2] : '';
    $_url = parse_url($url);
    $host = $_url['host'] ? $_url['host'] : 'www.sunfrogshirts.com';
    $query = $_url['host'] ? $_url['path'] : $url;
    $query = trim($query, '/');
    $url = "http://{$host}/{$query}";
    $posts = get_posts(array(
        'post_type' => 'post',
        'meta_query' => array(
            array(
                'key' => 'sunfrog_url',
                'value' => $url,
            ),
        ),
    ));
    // parse the url first
    if (count($posts) > 0) {

    } else {
        $src = get_curl($url);
        $name = trim(strip_tags(_cut3($src, '<h1>')));
        $img = _cut3($src, '<div class="productFrame');
        $img = 'http:' . cut($img, 'data-theImage="', '"', 0, 0);
        $desc = cut($src, '<input type="hidden" name="description"', '>');
        $desc = cut($desc, 'value="', '"', 0, 0);
        $price = _cut3($src, '<span class="priceshow"');
        $price = cut($price, '>', '</', 0, 0);
        $publish_time = get_option('last_published', time()) + get_option('public_delay', 300);
        delete_option('last_published');
        add_option('last_published', $publish_time);

        $content = get_option('spin_content');
        $content = str_replace(array(
            '{title}',
            '{price}',
            '{desc}',
            '{img}',
            '{url}',
            '{refID}',
        ), array(
            $name,
            $price,
            $desc,
            $img,
            $url,
            get_option('sunfrogID'),
        ), $content);

        while ($x = cut($content, '{', '}', 0, 0)) {
            $spinPieces = explode('|', trim($x, '{}'));
            $piece = $spinPieces[rand(0, count($spinPieces) - 1)];
            $content = str_replace('{' . $x . '}', $piece, $content);
        }

        $postID = wp_insert_post(array(
            'post_status' => 'future',
            'post_title' => $name,
            'post_content' => $content,
            'post_type' => 'post',
            'post_date' => date('Y-m-d H:i:s', $publish_time),
            'tags_input' => $tag,
            'post_category' => $cats
        ));
        if ($postID) {
            // update url
            add_post_meta($postID, 'sunfrog_url', $url);

            /**
             * <a href="<?php echo get_post_meta(get_the_ID(), 'sunfrog_url', true) ?>?42115">Buy Now</a>
             */
        }
    }
}

function add_settings_page___()
{
    add_menu_page('SunfrogShirts', 'SunfrogShirts', 'manage_options', 'SunfrogShirts', 'settings_page___', 'dashicons-admin-tools', 63);
}

function settings_page___()
{
    require_once "settings-cat.php";
}

add_action('admin_menu', 'add_settings_page___');
