<?php
// _add_product('http://www.sunfrogshirts.com/Awesome-Daughter-Shirt-Orange.html');
if ($_POST['submit'] == "Lưu CMMĐ") {
    if (isset($_POST['sunfrogID'])) {
        delete_option('sunfrogID');
        add_option('sunfrogID', $_POST['sunfrogID']);
    }

    if (isset($_POST['spin_content'])) {
        delete_option('spin_content');
        add_option('spin_content', $_POST['spin_content']);
    }
    ?>
    <div id="message" class="updated">
        <p>Cập nhật xong!</p>
    </div>
    <?php
}

?>
<div class="wrap">
    <h2>Cài đặt</h2>

    <form method="POST" action="admin.php?page=SunfrogShirts" enctype="multipart/form-data">
        <table class="form-table">
            <tr>
                <th scope="row"><label for="onetimelink">Sunfrog REF ID</label></th>
                <td><input name="sunfrogID" type="text" id="sunfrogID" class="regular-text"
                           value="<?php echo get_option('sunfrogID', 0) ?>"/></td>
            </tr>
            <tr>
                <th scope="row"><label for="spin_content">Content</label></th>
                <td>
                    <textarea cols="120" rows="20" name="spin_content"
                              id="spin_content"><?php echo get_option('spin_content') ?></textarea>
                </td>
            </tr>
        </table>
        <p class="submit"><input type="submit" name="submit" id="submit" class="button button-primary"
                                 value="Lưu CMMĐ"/></p>
    </form>
</div>